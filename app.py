from bs4 import BeautifulSoup
from lxml import etree
import requests
import config
from fastapi import FastAPI
import uvicorn

app = FastAPI()


def result_data(stock_name):
    url = f"https://www.investing.com/search/?q={stock_name}"
    headers = {
        "User-Agent": config.user_agen,
    }
    page = requests.get(url, headers=headers)
    soup = BeautifulSoup(page.content, "html.parser")
    find_name = soup.find_all("a", {"class": "js-inner-all-results-quote-item row"})
    for i in find_name:
        name = i.find("span", {"class": "second"}).text
        if stock_name.upper() == name:
            return get_data(i["href"].split("/")[2])
        else:
            return {"error": "stock not found"}


def get_data(stock):
    url = f"https://www.investing.com/equities/{stock}"
    headers = {
        "User-Agent": config.user_agen,
    }
    page = requests.get(url, headers=headers)
    try:
        soup = BeautifulSoup(page.content, "html.parser")
        dom = etree.HTML(str(soup))
        price_change = soup.find("span", {"data-test": config.price_change}).text
        price_change_percent = soup.find(
            "span", {"data-test": config.price_change_percent}
        ).text
        volume = dom.xpath(config.volume)[0].text
        bid = dom.xpath(config.bid)[0].text
        ask = dom.xpath(config.ask)[0].text
        day_range = dom.xpath(config.day_range)[0].text
        price_last = dom.xpath(config.price_last)[0].text
        return {
            "price_last": price_last,
            "price_change": price_change,
            "price_change_percent": price_change_percent.replace("(", "").replace(
                ")", ""
            ),
            "volume": volume,
            "bid_ask": f"{bid}/{ask}",
            "day_range": day_range,
        }
    except:
        return {"error": "stock not found"}


@app.get("/get_stock/{stock_name}")
def get_stock(stock_name):
    return result_data(stock_name)


if __name__ == "__main__":
    uvicorn.run("app:app", reload=True)
