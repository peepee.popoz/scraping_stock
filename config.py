user_agen = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36"
volume = '//*[@id="__next"]/div/div/div[2]/main/div/div[1]/div[2]/ul/li[1]/div[2]'
bid = '//*[@id="__next"]/div/div/div[2]/main/div/div[1]/div[2]/ul/li[2]/div[2]/span[1]'
ask = '//*[@id="__next"]/div/div/div[2]/main/div/div[1]/div[2]/ul/li[2]/div[2]/span[3]'
day_range = (
    '//*[@id="__next"]/div/div/div[2]/main/div/div[1]/div[2]/ul/li[2]/div[2]/span[3]'
)
price_last = '//*[@id="__next"]/div/div/div[2]/main/div/div[1]/div[2]/div[1]/span'
price_change = "instrument-price-change"
price_change_percent = "instrument-price-change-percent"
find_stock_name = "js-inner-all-results-quotes-wrapper newResultsContainer quatesTable"
# instrument-price_change-value__jkuml instrument-price_down__3dhtw
